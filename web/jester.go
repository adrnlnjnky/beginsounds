package main

import (
	"net/http"

	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/stream_jester"
)

type JesterPage struct {
	Name string
}

func JesterHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)

	jester := stream_jester.CurrentJester(db)
	p := player.FindByID(db, jester.PlayerID)
	page := JesterPage{Name: p.Name}

	jesterTmpl.Execute(w, page)
}
