package streamgod_router

import (
	"fmt"
	"strings"

	"gitlab.com/beginbot/beginsounds/pkg/audio_request"
	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/soundboard"
	"gitlab.com/beginbot/beginsounds/pkg/stream_jester"
	"gorm.io/gorm"
)

func PassTheJester(
	db *gorm.DB,
	msg chat.ChatMessage,
	parsedCmd *chat.ParsedCommand,
	chatResults chan<- string,
	aqs chan<- audio_request.AudioRequest,
) {

	jester := stream_jester.CurrentJester(db)

	if jester.PlayerID == 0 {
		chatResults <- "No Current Jester"
	}

	if jester.PlayerID == msg.PlayerID || msg.Streamgod {

		// This is for find the user
		// It looks like we aren't explictly
		// allowing users to pass the jester
		var p *player.Player
		var err error

		if parsedCmd.TargetUser != "" && parsedCmd.TargetUser != msg.PlayerName {
			p = player.Find(db, parsedCmd.TargetUser)
		} else {
			players := chat.CurrentlyChatting(db, msg.PlayerID)

			p, players, err = RandomPlayer(db, players)
			if err != nil {
				fmt.Printf("err = %+v\n", err)
			}
		}

		if jester.PlayerID == 0 {
			tx := db.Create(&jester)
			if tx.Error != nil {
				fmt.Printf("Error creating New Jester: %+v\n", tx.Error)
			}

			chatResults <- "No Current Jester"
		}

		fmt.Printf("Chosen Jester!: %v\n", p.Name)
		if err != nil {
			fmt.Printf("Error passing the Jester: %+v\n", err)
			return
		}

		fmt.Printf("Old Jester: %v\n", jester)

		tx := db.Model(&jester).Update("player_id", p.ID)
		if tx.Error != nil {
			fmt.Printf("Error Updating Jester: %+v\n", tx.Error)
		}
		aq, err := soundboard.CreateAudioRequest(db, "newjester", "beginbotbot")
		if err != nil {
			fmt.Printf("Error Creating Audio Request for New Jester: %+v\n", err)
			return
		}
		aqs <- *aq
		msg1 := strings.Join(stream_jester.Commands, " ")
		chatResults <- msg1
		msg := fmt.Sprintf("GlitchLit GlitchLit New Jester! @%s GlitchLit GlitchLit", p.Name)
		chatResults <- msg
	}
}
