package streamgod_router

import (
	"encoding/json"
	"fmt"
	"io/ioutil"

	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/stream_jester"
	"gitlab.com/beginbot/beginsounds/pkg/website_generator"
	"gorm.io/gorm"
)

func chaosMode(db *gorm.DB) string {
	jester := stream_jester.CurrentJester(db)
	tx := db.Model(&jester).Update("chaos_mode", !jester.ChaosMode)
	if tx.Error != nil {
		fmt.Printf("tx.err = %+v\n", tx.Error)
	}
	jesterName := player.FindByID(db, jester.PlayerID).Name
	go func() {
		data := map[string]interface{}{
			"chaos":  jester.ChaosMode,
			"jester": jesterName,
		}
		website_generator.JsonPage("wtf.json", data)
		jData, _ := json.Marshal(data)
		err := ioutil.WriteFile("build/wtf.json", jData, 0644)
		if err != nil {
			fmt.Printf("\nError Creating Build File: %s", err)
			return
		}
		website_generator.SyncJSONPage("wtf")
	}()
	// err = tmpl.Execute(f, page)
	return fmt.Sprintf("!zoombegin CHAOS MODE")
}
