package obs

import (
	"fmt"
	"math"
	"strconv"

	"gitlab.com/beginbot/beginsounds/pkg/memes"
)

// This is where we handle any massaging of settings
// from user inputs, until it hits OBS
func settingsBuilder(
	client *OBSClient,
	cmd string,
	obsCmd Command,
) map[string]interface{} {

	// We Find
	meme, _ := memes.FindDefault(client.DB, obsCmd.Source)
	settings := map[string]interface{}{
		"x":        meme.X,
		"start_x":  meme.X,
		"end_x":    meme.X,
		"y":        meme.Y,
		"start_y":  meme.Y,
		"end_y":    meme.Y,
		"rotation": meme.Rotation,
		"scale":    meme.Scale,
	}

	switch cmd {
	case "!spin":
		settings["delay"] = 5
	case "!scale":
		settings["scale"] = 1.0

		if len(obsCmd.Parts) > 2 {
			scale, err := strconv.ParseFloat(obsCmd.Parts[2], 64)

			if err != nil {
				return settings
			}

			if math.IsNaN(scale) || math.IsInf(scale, 0) {
				return settings
			}

			settings["scale"] = scale
		}

		fmt.Printf("Setting settings For Scale: %+v\n", settings)
		return settings
	case "!zoom":
		level := 200.0
		if len(obsCmd.Parts) > 2 {
			level, _ = strconv.ParseFloat(obsCmd.Parts[2], 64)
		}

		settings["level"] = level
	case "!slide":

		if meme.X < 400.0 {
			settings["start_x"] = 1200.0
		} else {
			settings["start_x"] = 0.0
		}

	case "!slideout":
		settings["end_x"] = -400.0
	case "!rotate":
		if obsCmd.Scene != "" && obsCmd.Source != "" {
			rotation := 0.0

			if len(obsCmd.Parts) > 2 {
				rotation, _ = strconv.ParseFloat(obsCmd.Parts[2], 64)
			}
			settings["rotation"] = rotation
		}

	case "!color":
		if len(obsCmd.Parts) > 2 {
			colorName := obsCmd.Parts[2]
			// We look for the color by name
			rawColor, ok := ColorCodes[colorName]
			if ok {
				color, _ := strconv.ParseFloat(rawColor, 64)

				fmt.Printf("colorName, = %v color %v\n", colorName, color)
				settings["color"] = color
			}

		}
	case "!rise":
		settings["start_y"] = 1000.0
	case "!unveil":
		settings["start_y"] = 400.0
	case "!fall":
		settings["start_y"] = -200.0
	case "!show":
		settings["toggle"] = true
	case "!hide":
		settings["toggle"] = false
	case "!move":
		if len(obsCmd.Parts) > 3 {
			rawX := obsCmd.Parts[2]
			rawY := obsCmd.Parts[3]
			x, _ := strconv.ParseFloat(rawX, 64)
			y, _ := strconv.ParseFloat(rawY, 64)
			settings["x"] = x
			settings["y"] = y
		}

	case "!flash", "!default":
		// This doesn't need to anything
		// the top of the switch statement
		// setting of default Meme does it all
		fmt.Println("Setting settings for flash: ", obsCmd.Source)
	}
	return settings
}
