package economy_router

import (
	"context"
	"fmt"
	"testing"
	"time"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/chat_parser"
	"gitlab.com/beginbot/beginsounds/pkg/database"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/stream_command"
	"gitlab.com/beginbot/beginsounds/pkg/test_support"
)

func TestBuyingAllAvailableSounds(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)

	commands := []string{"damn", "cool", "nice"}
	for _, c := range commands {
		sc := stream_command.StreamCommand{Name: c, Filename: fmt.Sprintf("%s.ops", c)}
		db.Create(&sc)
	}

	playername := "young.thug"
	p := player.Player{Name: playername, CoolPoints: 10}
	db.Create(&p)

	msg := "!buy 20"
	cm, _ := chat_parser.Parse(db, &p, msg)

	cmds := make(chan chat.ChatMessage, 1)
	cmds <- cm
	ctx := context.Background()
	results := BuyRoute(ctx, db, cmds)

	select {
	case <-time.NewTimer(time.Millisecond * 50).C:
		t.Error("No Message")
	case res := <-results:
		expectedMsg := "@young.thug spent all their money on damn, cool, nice: 3 Cool Points"
		if res != expectedMsg {
			t.Errorf("Error Parsing: %s", res)
		}
	}

}

func TestBuyingMoreThanYouCanAfford(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)

	commands := []string{"damn", "cool", "nice"}
	for _, c := range commands {
		sc := stream_command.StreamCommand{
			Name:     c,
			Filename: fmt.Sprintf("%s.ops", c),
			Cost:     4,
		}
		db.Create(&sc)
	}

	playername := "young.thug"
	p := player.Player{Name: playername, CoolPoints: 10}
	db.Create(&p)

	msg := "!buy 20"
	cm, _ := chat_parser.Parse(db, &p, msg)

	cmds := make(chan chat.ChatMessage, 1)
	cmds <- cm
	ctx := context.Background()
	results := BuyRoute(ctx, db, cmds)

	select {
	case <-time.NewTimer(time.Millisecond * 50).C:
		t.Error("No Message")
	case res := <-results:
		expectedMsg := "@young.thug bought: nice, cool for 8 Cool Points"
		if res != expectedMsg {
			t.Errorf("Error Parsing: %s", res)
		}
	}

}

func TestBuyingWithNoChoices(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)

	sc := stream_command.StreamCommand{
		Name:     "noice",
		Filename: "noice.opus",
		Cost:     4,
	}
	db.Create(&sc)

	playername := "young.thug"
	p := player.Player{Name: playername, CoolPoints: 3}
	db.Create(&p)

	msg := "!buy 20"
	cm, _ := chat_parser.Parse(db, &p, msg)

	cmds := make(chan chat.ChatMessage, 1)
	cmds <- cm
	ctx := context.Background()
	results := BuyRoute(ctx, db, cmds)

	select {
	case <-time.NewTimer(time.Millisecond * 50).C:
		t.Error("No Message")
	case res := <-results:
		expectedMsg := fmt.Sprintf(
			"No sounds found for purchase: @young.thug. Cool Points: %d",
			p.CoolPoints,
		)
		if res != expectedMsg {
			t.Errorf("Error Parsing: %s", res)
		}
	}

}
