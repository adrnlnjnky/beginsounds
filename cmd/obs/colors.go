package obs

var ColorCodes = map[string]string{
	"purple":    Purple,
	"yellow":    Yellow,
	"white":     White,
	"babyBlue":  BabyBlue,
	"lightBlue": LightBlue,
	"orange":    Orange,
	"red":       Red,
	"green":     Green,
	"brown":     Brown,
}

// This is OBS representing colors as a single float
// since it's "faster" than than RGB????
//
// Isn't that what HEX is for?
const (
	Purple    = "4.286513237e+09"
	Yellow    = "4.278255615e+09"
	White     = "4.294967295e+09"
	BabyBlue  = "4.294967125e+09"
	LightBlue = "4.294945365e+09"
	Orange    = "4.380255449e+09"
	Red       = "4.278190335e+09"
	Green     = "4.278255445e+09"
	Brown     = "4.278198887e+09"
)

var colors = []string{
	Purple,
	Yellow,
	White,
	BabyBlue,
	LightBlue,
	Orange,
	Brown,
	Red,
	Green,
}
