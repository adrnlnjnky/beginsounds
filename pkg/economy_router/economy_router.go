package economy_router

import (
	"context"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gorm.io/gorm"
)

func PermsRouter(
	ctx context.Context,
	db *gorm.DB,
	commands <-chan chat.ChatMessage,
) <-chan string {

	results := make(chan string)

	go func() {
		defer close(results)

	MsgLoop:
		for msg := range commands {
			parsedCmd := msg.ParsedCmd

			// We need to generate webpage on perm check
			switch parsedCmd.Name {
			case "perms":

				isTheme := parsedCmd.TargetCommand != parsedCmd.TargetUser
				if parsedCmd.TargetCommand != "" && isTheme {
					res := CommandInfo(db, msg, parsedCmd.TargetCommand)
					results <- res
					continue MsgLoop
				}

				results <- UserInfo(db, parsedCmd.TargetUser)
				continue MsgLoop
			}

		}
	}()

	return results
}
