package obs

// Command - represents what a OBS command needs to be called
type Command struct {
	Scene    string
	Source   string
	Filters  []string
	Parts    []string
	Settings map[string]interface{}
	Func     transformFunc
}

type transformFunc = func(
	*OBSClient,
	string, // Scene
	string, // Source
	map[string]interface{}, // Settings
) error

// FilterMap The set of filters the effect uses
// 		for every source, and value we have 1 channel
//    that blocks, so we only trigger effect at time
var FilterMap = map[string][]string{
	"!spin":     {"transform"},
	"!scale":    {"transform"},
	"!zoom":     {"transform"},
	"!slide":    {"position"},
	"!slideout": {"position"},
	"!rotate":   {"rotate"},
	"!chad":     {"transform"},
	"!color":    {"color_correction"},
	"!peak":     {"position"},
	"!peek":     {"position"},
	"!zoomout":  {"transform"},
	"!move":     {"position"},
	"!fall":     {"position"},
	"!rise":     {"position"},
	"!unveil":   {"tranform"},
	"!show":     {"position"},
	"!hide":     {"position"},
	"!flash":    {"position"},
	"!default":  {"position"},

	// "!find": {"transform"},
	// "!hide": {"transform"},
	// "!zoom_out": {"transform"},
	// "!zs": {"transform"},
	// "!movex": {"transform"},
}

// CmdFuncMap what function to run for each command
var CmdFuncMap = map[string]transformFunc{
	"!spin":     Spin,
	"!scale":    ScaleSource,
	"!zoom":     Zoom,
	"!slide":    SlideSource,
	"!slideout": SlideOutSource,
	"!peak":     SlideInFromSide,
	"!peek":     SlideInFromSide,
	"!rotate":   RotateSource,
	"!chad":     BigBrain,
	"!color":    FadeToColor,
	"!fall":     FallSource,
	"!rise":     RiseSource,
	"!zoomout":  ZoomOut,
	"!unveil":   UnveilSource,
	"!show":     ShowSource,
	"!hide":     HideSource,
	"!move":     MoveSource,
	"!flash":    FlashSource,
	"!default":  DefaultSource,
}
