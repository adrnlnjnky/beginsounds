CREATE TABLE public.stream_jesters (
    id SERIAL,
    secret character varying(255) NOT NULL,
    filename character varying(255) NOT NULL,
    chaos_mode bool DEFAULT false NOT NULL,
    player_id int references players(id),
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    deleted_at timestamp with time zone,
    CONSTRAINT stream_jesters_pkey PRIMARY KEY (id)
);
