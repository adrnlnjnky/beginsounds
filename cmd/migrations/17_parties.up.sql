CREATE TABLE public.parties (
    id SERIAL,
    name character varying(255) NOT NULL,
    manifesto text NOT NULL,
    approved bool DEFAULT false,
    leader_id int references players(id) NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    CONSTRAINT parties_pkey PRIMARY KEY (id)
);
