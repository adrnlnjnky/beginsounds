package obs

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"strings"
	"sync"
	"time"

	obsws "github.com/davidbegin/go-obs-websocket"
	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/stream_jester"
	"gorm.io/gorm"
)

var lordCommands = []string{
	"!flash",
}

type OBSClient struct {
	Client *obsws.Client
	Mutex  *sync.Mutex
	Logger *log.Logger
	DB     *gorm.DB
}

// TrollBegin is the main loop processing user messages
//   to "troll" Begin and trigger various OBS effects
//   with Websockets
func TrollBegin(
	db *gorm.DB,
	commands <-chan chat.ChatMessage,
) <-chan string {

	results := make(chan string, 10000)

	go func() {
		defer close(results)

		logger := log.New(ioutil.Discard, "obsws2 ", log.LstdFlags)
		c := obsws.Client{Host: "localhost", Port: 4444, Logger: logger}
		if err := c.Connect(); err != nil {
			log.Print(err)
		}
		defer c.Disconnect()

		var mutex = &sync.Mutex{}
		f, err := os.OpenFile("tmp/obs_errors.txt", os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0644)
		if err != nil {
			log.Fatal(err)
		}
		errLogger := log.New(f, "Error: ", log.LstdFlags)

		client := OBSClient{
			Client: &c,
			Mutex:  mutex,
			Logger: errLogger,
			DB:     db,
		}

		explodedCommands := chatExploder(commands)

		sourceFilterChannels := make(map[string](chan Command))

	Loop:
		// for msg := range commands {
		for msg := range explodedCommands {
			// Do we need to call this on every loop?
			rand.Seed(time.Now().UnixNano())

			p := findPlayer(db, msg)
			if p.ID == 0 {
				continue Loop
			}

			// TODO: Should we include a message here?
			if p.InJail {
				continue Loop
			}

			// We Will need to use this mana one day again
			// mana := p.Mana

			msgBreakdown := strings.Split(msg.Message, " ")
			cmd := strings.ToLower(strings.TrimSpace(msgBreakdown[0]))

			// We allow everyone to return Begin to Normie
			if cmd == "!normie" {
				ReturnToNormie(&client)
				ResetTransform(&client, "Alerts")
				ResetTransform(&client, "keyboard")
				ResetTransform(&client, "Chat")
				continue Loop
			}

			if cmd == "!clippy" && msg.Streamlord {
				advice := strings.Join(msgBreakdown[1:], " ")
				f, err := os.Create("tmp/clippy.txt")
				if err != nil {
					fmt.Printf("Error Creating Clippy Advice %+v\n", err)
					continue Loop
				}
				_, err = io.WriteString(f, advice)
				if err != nil {
					fmt.Printf("Error WRiting Clippy Advice %+v\n", err)
					continue Loop
				}
			}

			for _, specialCmd := range lordCommands {
				if cmd == specialCmd {

					if !msg.Streamgod && !msg.Streamlord {
						continue Loop
					}
				}
			}

			// TODO: Clean this logic up
			// Check if the Jester is current player
			// or if we are in Chaos Mode
			jester := stream_jester.CurrentJester(db)
			if !jester.ChaosMode && !msg.Streamgod {
				if jester.Secret == cmd[1:] && jester.PlayerID == 0 {
					db.Model(&jester).Update("player_id", msg.PlayerID)
					results <- fmt.Sprintf("New Jester! @%s", msg.PlayerName)
					waitForJester := time.NewTimer(1 * time.Second)
					<-waitForJester.C
					ChangeScene(&client, "jester_scene")
					continue Loop
				}
				if jester == nil || jester.PlayerID == 0 || jester.PlayerID != p.ID {
					continue Loop
				}
			}

			if cmd == "!passthejester" {
				waitForJester := time.NewTimer(500 * time.Millisecond)
				<-waitForJester.C
				ChangeScene(&client, "jester_scene")
				continue Loop
			}

			resultsChannels := make(map[string](<-chan string))
			obsCmd := findSourceAndScene(msg.Message)
			obsCmd.Settings = settingsBuilder(&client, cmd, obsCmd)

			for _, filter := range obsCmd.Filters {
				chanKey := fmt.Sprintf("%s+%s", obsCmd.Source, filter)
				_, ok := sourceFilterChannels[chanKey]
				if !ok {
					channel := make(chan Command)
					sourceFilterChannels[chanKey] = channel
					results := ProcessObsCommands(&client, channel)
					resultsChannels[chanKey] = results
				}
			}

			for _, filter := range obsCmd.Filters {
				chanKey := fmt.Sprintf("%s+%s", obsCmd.Source, filter)
				channel, ok := sourceFilterChannels[chanKey]
				f, funcFound := CmdFuncMap[cmd]
				if ok && funcFound {
					obsCmd.Func = f
					channel <- obsCmd
				}
			}

			// RandomRouter(&client, cmd, obsCmd, results)
			obsEffectFuncs := []transformFuncCollection{
				UserEffects,
				BeginEffects,
				FilterEffects,
				ProcessNormalizeRequests,
				ProcessHelpers,
				ProcessEpicEffects,
			}

			var cost int
			for _, f := range obsEffectFuncs {
				triggered, _ := f(&client, cmd, obsCmd, results)
				if triggered {
					cost++
				}
			}

			tsc, ok := ToggleSources[cmd]
			if ok {
				cost++

				ToggleSource(
					&client,
					tsc.Scene,
					tsc.Source,
					tsc.Toggle,
				)
			}

			type obsSourceFunc = func(*OBSClient, *gorm.DB, string, Command, <-chan string) (bool, error)
			obsSourceFuncs := []obsSourceFunc{
				MemeSaver,
				ProcessCreateSourcesRequests,
			}
			for _, f := range obsSourceFuncs {
				triggered, _ := f(&client, db, cmd, obsCmd, results)
				if triggered {
					cost++
				}
			}

			if jester.ChaosMode && !msg.Streamgod && cost > 0 {
				fmt.Printf("\t$$$ Charging Name: %+v | Mana: %d\n", p.Name, cost)
				p.UpdateMana(db, cost)
			}

		}

	}()

	obsws.SetReceiveTimeout(time.Second * 2)
	return results
}
