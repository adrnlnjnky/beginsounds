package pack

import (
	"fmt"
	"os"
	"text/template"

	"gorm.io/gorm"
)

type PacksPage struct {
	// Metadata
	Domain    string
	Extension string
	Packs     []PackResult
}

func GeneratePacksPage(db *gorm.DB) {
	packs_tmpl := template.Must(template.ParseFiles(
		"templates/packs.html",
		"templates/sub_templates.tmpl",
	))
	ps, err := All(db)
	if err != nil {
		fmt.Printf("Error Fetching All Packs: %+v\n", err)
	}

	remotePage := PacksPage{
		// Domain:    "https://beginworld.website-us-east-1.linodeobjects.com",
		Domain:    "http://beginworld.exchange",
		Extension: ".html",
		Packs:     ps,
	}
	buildFile := fmt.Sprintf("build/packs.html")
	f, err := os.Create(buildFile)
	if err != nil {
		fmt.Printf("\nError Creating Build File: %s", err)
		return
	}
	err = packs_tmpl.Execute(f, remotePage)
	if err != nil {
		fmt.Printf("err = %+v\n", err)
	}

	// We should sync here as well
}
