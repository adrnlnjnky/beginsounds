CREATE TABLE public.parties_players (
    id SERIAL,
    player_id int references players(id),
    party_id int references parties(id),
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    CONSTRAINT parties_players_pkey PRIMARY KEY (id),
    CONSTRAINT parties_players_unique UNIQUE(player_id,party_id)
);
