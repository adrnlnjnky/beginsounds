package audio_authorizer

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

type PythonAuthorizer struct{}

// I called this Controlled, because FakeAuthorizer
// hurt me, being in the "regular" code
type ControlledAuthorizer struct {
	IsAllowed  bool
	Streamlord bool
}

func (a *ControlledAuthorizer) String() string {
	return fmt.Sprintf("<ControlledAuthorizer Allowed: %t | Streamlord: %t>", a.IsAllowed, a.Streamlord)
}

func (a *ControlledAuthorizer) FetchAuthorization(sfx string, username string) AudioAuthorizerResponse {
	response := AudioAuthorizerResponse{
		IsAllowed:  a.IsAllowed,
		Streamlord: a.Streamlord}

	return response
}

func (a AudioAuthorizerResponse) String() string {
	return fmt.Sprintf("\tAuthorizer: Allowed: %t | Streamlord: %t | Mana: %d | Owned: %t", a.IsAllowed, a.Streamlord, a.Mana, a.Owned)
}

type AudioAuthorizerResponse struct {
	IsAllowed  bool `json:"allowed"`
	Streamlord bool `json:"streamlord"`
	Mana       int  `json:"mana"`
	Owned      bool `json:"owned"`
}

type Authorizer interface {
	FetchAuthorization(string, string) AudioAuthorizerResponse
}

func (a *AudioAuthorizerResponse) Allowed() {
	fmt.Println("Allowed: ", a.IsAllowed)
}

func (a *PythonAuthorizer) FetchAuthorization(sfx string, username string) AudioAuthorizerResponse {
	var response AudioAuthorizerResponse

	// if username == "beginbotbot" || username == "beginbot" {
	if username == "beginbot" {
		response.IsAllowed = true
		response.Streamlord = true
		return response
	}

	host := "http://localhost:1989"
	path := fmt.Sprintf("sound/%s/allowed/%s", sfx, username)
	sound_url := fmt.Sprintf("%s/%s", host, path)
	resp, err := http.Get(sound_url)

	// err could never not be nil???
	if err != nil {
		return response
		fmt.Println("Audio Authorizer Server is Down, allowing all in.")
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal("CRASH IT ALL: ", err)
	}
	err = json.Unmarshal(body, &response)
	fmt.Printf("%s\n", response)
	return response
}
