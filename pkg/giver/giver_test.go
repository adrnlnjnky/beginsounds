package giver

import (
	"fmt"
	"testing"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/database"
	"gitlab.com/beginbot/beginsounds/pkg/permissions"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/stream_command"
	"gitlab.com/beginbot/beginsounds/pkg/test_support"
)

var testData = []struct {
	input       chat.ChatMessage
	output      string
	ownsCommand bool
}{
	{},
}

// player doesn't own the command
// other player already owns command
// success
func TestGiver(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)
	yt := player.CreatePlayerFromName(db, "young.thug")
	g := player.CreatePlayerFromName(db, "gunna")
	damn := stream_command.CreateFromName(db, "damn")
	permissions.Allow(db, yt.ID, damn.ID)

	bChan := make(chan string)
	bbChan := make(chan string)

	gifterBalancer := g.CoolPoints
	cm := chat.ChatMessage{
		PlayerName: "young.thug",
		PlayerID:   yt.ID,
		Message:    "!give @gunna damn",
		ParsedCmd: chat.ParsedCommand{
			TargetUser:      g.Name,
			TargetUserID:    g.ID,
			TargetCommand:   damn.Name,
			TargetCommandID: damn.ID,
		},
		Parts: []string{"!give", "@gunna", "damn"},
	}

	res, err := Giver(db, bbChan, bChan, cm)
	isAllowed := permissions.IsAllowed(db, g.ID, damn.ID)
	if !isAllowed {
		t.Error(
			fmt.Sprintf(
				"We did not give !damn to @gunna: Response: %s | Error: %+v",
				res,
				err,
			),
		)
	}

	newGifterBalance := player.Find(db, "gunna").CoolPoints
	if gifterBalancer != newGifterBalance {
		t.Errorf("We are changing the Gifters Balance: %d", gifterBalancer-newGifterBalance)
	}
	// expectedOutput := "@young.thug gave @gunna !damn",
}

func TestYouCannotGiveWhatYouDoNotOwn(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)
	yt := player.CreatePlayerFromName(db, "young.thug")
	g := player.CreatePlayerFromName(db, "gunna")
	damn := stream_command.CreateFromName(db, "damn")
	permissions.AllowUserAccessToCommand(db, yt.Name, damn.Name)
	bChan := make(chan string)
	bbChan := make(chan string)

	gifterBalancer := g.CoolPoints
	cm := chat.ChatMessage{
		PlayerName: "young.thug",
		Message:    "!give @gunna damn",
		ParsedCmd: chat.ParsedCommand{
			TargetUser:      g.Name,
			TargetUserID:    g.ID,
			TargetCommand:   damn.Name,
			TargetCommandID: damn.ID,
		},
		Parts: []string{"!give", "@gunna", "damn"},
	}

	res, err := Giver(db, bbChan, bChan, cm)
	isAllowed := permissions.IsAllowed(db, g.ID, damn.ID)
	if isAllowed {
		t.Error(
			fmt.Sprintf(
				"We did give !damn to @gunna: Response: %s | Error: %+v",
				res,
				err,
			),
		)
	}

	newGifterBalance := player.Find(db, "gunna").CoolPoints
	if gifterBalancer != newGifterBalance {
		t.Errorf("We are changing the Gifters Balance: %d", gifterBalancer-newGifterBalance)
	}
	// expectedOutput := "@young.thug gave @gunna !damn",
}
