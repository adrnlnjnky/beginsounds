package obs

import (
	obsws "github.com/davidbegin/go-obs-websocket"
)

// Request - A Request to OBS
type Request interface {
	Send(obsws.Client) error
	Receive() (obsws.Response, error)
}

func (client *OBSClient) execOBSCommand(req Request) (obsws.Response, error) {
	client.Mutex.Lock()
	defer client.Mutex.Unlock()

	if err := req.Send(*client.Client); err != nil {
		client.Logger.Println(err)
	}
	resp, err := req.Receive()
	if err != nil {
		client.Logger.Println(err)
	}
	return resp, err
}

func setSourceFilterSettings(
	client *OBSClient,
	source string,
	filterName string,
	settings map[string]interface{},
) {
	req := obsws.NewSetSourceFilterSettingsRequest(source, filterName, settings)
	client.execOBSCommand(&req)
}

func processMoveSceneItem(
	client *OBSClient,
	req obsws.Request,
) (obsws.SetSceneItemPositionResponse, error) {

	resp, err := client.execOBSCommand(req)
	r := resp.(obsws.SetSceneItemPositionResponse)
	return r, err
}

func toggleTransform(client *OBSClient, sceneName string, toggle bool) {
	req := obsws.NewSetSourceFilterVisibilityRequest(sceneName, "transform", toggle)
	client.execOBSCommand(&req)
}
