package obs

import (
	"fmt"
	"io/ioutil"
	"log"
	"testing"

	obsws "github.com/davidbegin/go-obs-websocket"
	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/database"
	"gitlab.com/beginbot/beginsounds/pkg/memes"
	"gitlab.com/beginbot/beginsounds/pkg/player"
)

func NotTestSavingMeme(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	p := player.CreatePlayerFromName(db, "young.thug")
	logger := log.New(ioutil.Discard, "obsws2 ", log.LstdFlags)
	c := obsws.Client{Host: "localhost", Port: 4444, Logger: logger}
	if err := c.Connect(); err != nil {
		log.Print(err)
	}
	defer c.Disconnect()
	// var mutex = &sync.Mutex{}
	// source := "carlvan"

	// client := OBSClient{
	// 	Client: &c,
	// 	Mutex:  mutex,
	// 	Logger: logger,
	// }
	// obsMeme := MemeInfo(&client, source)

	msg := "!save carlvan"

	parsedCmd := chat.ParsedCommand{
		Name: "save",
		// TargetMedia     string
	}

	// we need a message that says save a meme
	// and we need to make sure it saves a Meme
	cm := chat.ChatMessage{
		PlayerName: p.Name,
		PlayerID:   p.ID,
		Message:    msg,
		Streamgod:  true,
		ParsedCmd:  parsedCmd,
		Parts:      []string{"!save", "carlvan"},
	}

	fmt.Printf("cm = %+v\n", cm)

	m, _ := memes.Find(db, "carlvan")
	if m.X != 700.0 {
		t.Errorf("Error Saving Meme: %+v", m)
	}
}
