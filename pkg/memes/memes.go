package memes

import (
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type Meme struct {
	ID           uint
	Name         string
	X            float64
	Y            float64
	Scale        float64
	Rotation     float64
	PositionType string
}

func (m *Meme) Enable(db *gorm.DB) error {
	tx := db.Model(m).Update("enable", true)
	return tx.Error
}

func (m *Meme) Disable(db *gorm.DB) error {
	tx := db.Model(m).Update("enable", false)
	return tx.Error
}

func All(db *gorm.DB) ([]*Meme, error) {
	var res []*Meme
	tx := db.Find(&res)
	return res, tx.Error
}

func FindDefault(db *gorm.DB, name string) (*Meme, error) {
	var m Meme
	tx := db.Where("name = ? AND position_type = 'default'", name).First(&m)
	return &m, tx.Error
}

func Find(db *gorm.DB, name string) (*Meme, error) {
	var m Meme
	tx := db.Where("name = ?", name).First(&m)
	return &m, tx.Error
}

func (m *Meme) Save(db *gorm.DB) error {
	tx := db.Clauses(clause.OnConflict{
		Columns:   []clause.Column{{Name: "name"}, {Name: "position_type"}},
		DoUpdates: clause.AssignmentColumns([]string{"x", "y", "scale", "rotation"}),
	}).Create(&m)
	return tx.Error
}
