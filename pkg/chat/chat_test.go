package chat

import (
	"fmt"
	"testing"

	"gitlab.com/beginbot/beginsounds/pkg/database"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/test_support"
)

func TestSave(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)
	p := player.CreatePlayerFromName(db, "beginbot")

	ct := ChatMessage{PlayerID: p.ID, PlayerName: p.Name, Message: "Hello"}
	Save(db, ct)
	count := Count(db)

	if count != 1 {
		t.Error("Didn't save message")
	}
}

// // I bet this is failing
// func TestSaveChat(t *testing.T) {
// 	db := database.CreateDBConn("test_beginsounds3")
// 	test_support.ClearDb(db)

// 	username := "beginbot"

// 	ctx := context.Background()
// 	// Are we ending up with more than 1 channel
// 	datastream := make(chan ChatMessage, 1)

// 	// beginbot
// 	// and damn need to exist I think
// 	player.CreatePlayerFromName(db, username)
// 	stream_command.CreateFromName(db, "damn")
// 	stream_command.CreateFromName(db, "beginbot")
// 	cm := ChatMessage{PlayerName: username, Message: "!damn"}
// 	datastream <- cm

// 	audioRequests := SaveChat(ctx, db, datastream)

// 	timer := time.NewTimer(50 * time.Millisecond)
// 	select {
// 	case _ = <-timer.C:
// 		t.Error("We Expected an Audio Request")
// 	case res := <-audioRequests:
// 		if res.Name != username {
// 			t.Errorf("We are not processing Audio Requests properly: %+v", res)
// 		}
// 	}

// 	// cm2 := ChatMessage{PlayerName: username, Message: "!damn"}
// 	// datastream <- cm2
// 	// timer := time.NewTimer(2 * time.Second)
// 	// ars := SaveChat(ctx, db, datastream)
// 	// TODO: This is terrible:
// 	// Figure out the  Gopher way to handle this
// 	// time.Sleep(time.Millisecond * 50)

// 	// Ars never gets written to
// 	// select {
// 	// case ar1 := <-ars:
// 	// 	t.Errorf("Uh we shouldn't get an audio request: %+v", ar1)
// 	// // case _ = <-timer:
// 	// // 	t.Error("We fucked up somehow")
// 	// default:
// 	// }
// }

func TestHasChattedToday(t *testing.T) {
	username := "beginbot"
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)
	chattedToday := HasChattedToday(db, username)
	p := player.CreatePlayerFromName(db, "beginbot")

	if chattedToday {
		t.Errorf("%s should NOT have chatted today", username)
	}

	cm := ChatMessage{PlayerID: p.ID, PlayerName: username, Message: "!damn"}
	Save(db, cm)

	chattedToday = HasChattedToday(db, username)
	if !chattedToday {
		t.Errorf("%s should have chatted today", username)
	}
}

// func TestRecentChatters(t *testing.T) {
// 	db := database.CreateDBConn("beginsounds4")
// t/ 	res := RecentChatters(db)
// 	fmt.Printf("res = %+v\n", res)
// 	// Create 2 chat messages here,
// 	// one older than 4 hours
// }

func TestGiveStreetCredToRecentChatters(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)

	res := FetchStreetCredCounts(db)
	fmt.Printf("res = %+v\n", res)

	for _, r := range res {
		fmt.Printf("r = %+v\n", r)
		tx := db.Exec(`
			UPDATE players
			SET street_cred = street_cred + ?
			WHERE id = ?`,
			r.LoverCount, r.ID)
		if tx.Error != nil {
			fmt.Printf("res.Error = %+v\n", tx.Error)
		}
	}

	// if res.Error != nil {
	// 	fmt.Printf("res.Error = %+v\n", res.Error)
	// }
	// Now we can iterate through an update each use
	// var results []*StreetCredMap

	// db.Table("players").Raw(`
	// SELECT p.name, (count(pl) + 1) as lover_count FROM players p
	// LEFT JOIN players_lovers pl ON p.ID = pl.lover_id
	// INNER JOIN (
	//   SELECT
	//     COUNT(1), player_id
	//     FROM chat_messages
	//     WHERE chat_messages.created_at < (NOW() + interval '4 hour')
	//     GROUP BY player_id
	// ) cm ON cm.player_id = p.id
	// GROUP BY 1
	// ORDER BY lover_count DESC
	// `, username).Scan(&results)
	// return results

	// return count == 1
	// We need to get all users who have chatted recently
	// Plus number of people who Love them
	// We can then use that to give more street_cred to everyone

	// SELECT p.name, count(pl.id) AS lover_count FROM players p
	// RIGHT JOIN players_lovers pl ON p.ID = pl.lover_id
	// GROUP BY p.name, pl.id;
}
