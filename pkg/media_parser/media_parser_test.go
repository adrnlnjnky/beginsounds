package media_parser

import (
	"testing"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
)

var mediaTests = []struct {
	n      string
	output string
}{
	{"!gif https://media1.tenor.com/images/tenor.gif?itemid=14291289 dotheurkel", "dotheurkel.gif"},
	{"!gif https://media1.tenor.com/images/tenor.gif dotheurkel", "dotheurkel.gif"},
	{"!gif https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fmedia.giphy.com%2Fmedia%2Fxlluygfq02MbC%2Fgiphy.gif&f=1&nofb=1 spideywall", "spideywall.gif"},
}

func TestParsing(t *testing.T) {
	for _, tt := range mediaTests {
		cm := chat.ChatMessage{
			PlayerName: "young.thug",
			Streamgod:  true,
			Message:    tt.n,
		}
		resp, err := Parse("video", cm)
		if err != nil {
			t.Error(err)
		}

		if resp.Filename != tt.output {
			t.Error("Incorrect Filename: ", resp.Filename)
		}
	}
}
