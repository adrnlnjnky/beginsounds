package economy_router

import (
	"context"
	"fmt"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/giver"
	"gorm.io/gorm"
)

func GivingRouter(
	ctx context.Context,
	db *gorm.DB,
	messages <-chan chat.ChatMessage,
) (chan string, chan string) {

	beginbotResponses := make(chan string)
	beginbotbotResponses := make(chan string)

	go func() {
		defer close(beginbotResponses)
		defer close(beginbotbotResponses)

		for msg := range messages {
			switch msg.ParsedCmd.Name {
			// Giving / Donating gives away command
			// at no cost
			case "give", "donate":
				msg, err := giver.Giver(
					db,
					beginbotbotResponses,
					beginbotResponses,
					msg,
				)
				if err == nil {
					beginbotResponses <- msg
				} else {
					beginbotbotResponses <- fmt.Sprintf("Error Giving: %+v", err)
				}

			// Cloning/Duplicating Costs the current command cost
			// and then doubles the cost of the command
			case "clone", "duplicate":
				beginbotResponses <- fmt.Sprintf("!%s COMING SOON!", msg.ParsedCmd.Name)
				// This requires other users agreeing
				// Do this later
				// case "sell"
			}
			// switch msg.C
		}
	}()

	return beginbotbotResponses, beginbotResponses
}
